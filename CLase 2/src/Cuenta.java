
public class Cuenta {
	private double saldo;
	private Cliente titular;
	private Moneda moneda;
	
	public Cuenta(Cliente t, Moneda m) {
		titular = t;
		moneda = m;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void depositar() {
		
	}
	
	public void extraer() {
		
	}
	
	public void calcularInteresMensual() {
		
	}
}
