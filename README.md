# Clase 2 - Clases, Colaboraciones y Mensajes

En esta clase se tratan los siguientes conceptos sobre POO:

- Referencia
- Igualdad e Identidad
- Asignación de Referencia 
- Aliasing 
- Copia de Obejos (Copia profunda?) Semántica de las referencias 
- Referencias vs Punteros 
- Metodo y Mensaje 
- Mensaje vs Procedimientos 
- Definición de método  
- Sobrecarga de métodos 
- Parametros formales y reales 
- Paso de parámetros 
- Instancia actual 
- This 
- Creacion de Objetos 
- Declaración vs Creacion